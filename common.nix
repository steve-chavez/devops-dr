{region, accessKeyId, pkgs, secrets, config, shouldRecover, pgPkg, resources, isActive, ...}: {
  deployment = {
    targetEnv = "ec2";
    ec2 = {
      inherit region accessKeyId;
      instanceType             = "t2.nano";
      associatePublicIpAddress = true;
      ebsInitialRootDiskSize   = 10;
      keyPair                  = resources.ec2KeyPairs.keyPair;
      securityGroups           = [resources.ec2SecurityGroups.secGroup];
    };
  };

  systemd.services.dataDirRemover = pkgs.lib.mkIf (!isActive) {
    wantedBy = ["multi-user.target"];
    description = "Remove pg data dir";
    serviceConfig =
    let script = pkgs.writeShellScript "removepgdatadir" ''
      set -euo pipefail

      rm -rf /var/lib/postgresql/12/
    '';
    in
    {
      Type = "oneshot";
      ExecStart = "${pkgs.bash}/bin/bash ${script}";
    };
  };

  systemd.services.basebackupToS3 = pkgs.lib.mkIf isActive {
    environment = secrets;
    description = "Take a pg basebackup and send it to AWS S3";
    serviceConfig =
    let script = pkgs.writeShellScript "basebackupToS3" ''
      set -euo pipefail

      # Delete tmp dir(otherwise pg_basebackup refuses to overwrite)
      rm -rf /tmp/basebackup

      ${pgPkg}/bin/pg_basebackup --wal-method=none --format=tar --gzip --username postgres -D /tmp/basebackup

      # Find latest wal segment to name base backup after it
      latest_wal=$(ls -tr ${config.services.postgresql.dataDir}/pg_wal/ | grep backup | head -n1 | sed -e 's/\..*$//')

      bak_name=$latest_wal.base.tar.gz

      echo "Sending: $bak_name"

      ${pkgs.awscli}/bin/aws s3 cp /tmp/basebackup/base.tar.gz s3://${resources.s3Buckets.backups.name}/$bak_name
    '';
    in
    {
      ExecStart = "${pkgs.bash}/bin/bash ${script}";
    };
  };

  systemd.timers.basebackupToS3 = pkgs.lib.mkIf isActive {
    enable      = true;
    after       = [ "postgresql.service" ];
    wantedBy    = [ "multi-user.target" ];
    unitConfig = {
      Description = "Run basebackupToS3 every day at 02:00 am";
    };
    timerConfig = {
      OnCalendar = "*-*-* 13:00:00";
      Unit = "basebackupToS3.service";
    };
  };

  systemd.services.postgresql =
  let
    cfg = config.services.postgresql;
    s3Bucket = "s3://${resources.s3Buckets.backups.name}";
    configFile =
      let
        toStr = value:
          if true == value then "yes"
          else if false == value then "no"
          else if pkgs.lib.isString value then "'${pkgs.lib.replaceStrings ["'"] ["''"] value}'"
          else toString value;
      in
      pkgs.writeText "postgresql.conf" (pkgs.lib.concatStringsSep "\n" (pkgs.lib.mapAttrsToList (n: v: "${n} = ${toStr v}") cfg.settings));
  in
  pkgs.lib.mkIf isActive
  {
    # We use path instead of inline definition(like "${pkgs.tar}/bin/tar}") because gzip cannot be found: https://github.com/NixOS/nixpkgs/issues/28527#issuecomment-325182680
    path = with pkgs; [ gnutar gzip awscli gawk ];
    environment = secrets;
    preStart = if (!shouldRecover)
    then ""
    else pkgs.lib.mkForce
    ''
    if ! test -e ${cfg.dataDir}/PG_VERSION; then

    echo "Starting recovery"

    cd ${cfg.dataDir}

    echo "Obtaining latest base bacukp"

    latestBase=$(aws s3 ls ${s3Bucket} | grep base | tail -n 1 | awk '{print $4}')

    echo "Downloading base backup"

    aws s3 cp ${s3Bucket}/$latestBase .

    tar xvf $latestBase

    cd pg_wal

    echo "Downloading WALs"

    while read f; do
      echo "Downloading $f"
      aws s3 cp ${s3Bucket}/$f .
    done <<EOF
      $(aws s3 ls ${s3Bucket} | awk '{print $4}' | sed -e "1,/$latestBase/ d")
    EOF

    gzip -d *.gz

    cd ${cfg.dataDir}

    echo "Adding recovery.signal"

    touch recovery.signal

    rm $latestBase

    echo "Recovery done"

    fi

    echo "Adding postgresql.conf"

    ln -sfn ${configFile} ${cfg.dataDir}/postgresql.conf

    '';
  };

  networking.firewall.allowedTCPPorts = [ 5432 ];
}
