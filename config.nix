{
  # Instante de tiempo al que se quiere recuperar, solo se ejecutará si se cambia la instancia.
  recoveryTime = "2021-02-06 07:50:00";

  # La instancia de base de datos, solo puede "primary" o "secondary"
  instance = "secondary";

  # Tiempo de expiración de los backups en días
  expirationTime = "30";

  # Tiempo maximo de perdida de datos, en segundos. Esto es equivalente al RPO.
  walTimeout = 60;
}
