let
  mainConfig = import ./config.nix;

  instance = mainConfig.instance;
  recoveryTime = mainConfig.recoveryTime;
  expirationTime = mainConfig.expirationTime;
  walTimeout = mainConfig.walTimeout;

  region = "us-east-2";
  accessKeyId = builtins.getEnv "EC2_ACCESS_KEY";
  secretKey = builtins.getEnv "EC2_SECRET_KEY";
  secrets = {
    AWS_ACCESS_KEY_ID = accessKeyId;
    AWS_SECRET_ACCESS_KEY = secretKey;
  };

  localTime = "America/Lima";

  primary = instance == "primary";
  secondary = instance == "secondary";
  shouldRecover = if recoveryTime == "" then false else true;

  pkgs = import <nixpkgs> {};
  pgPkg = pkgs.postgresql_12;
  pgrst = import ./pgrst.nix { stdenv = pkgs.stdenv; fetchurl = pkgs.fetchurl; };
  common = import ./common.nix;
in
with pkgs;
{
  network.description = "Devops disaster recovery";

  ##Provisioning
  resources = {
    ec2KeyPairs.keyPair = {
      inherit region accessKeyId;
    };

    # s3Buckets.<name>
    # <name> must be unique, otherwise you'll get misleading error https://github.com/aws/aws-cli/issues/2603
    # Also don't use camelcase or you'll get:
    # botocore.exceptions.ClientError: An error occurred (InvalidBucketName) when calling the CreateBucket operation: The specified bucket is not valid.
    s3Buckets.backups = {
      inherit region accessKeyId;
      versioning = "Suspended";
      lifeCycle = ''
         {
           "Rules": [
              {
                "ID": "Expire",
                "Status": "Enabled",
                "Prefix": "",
                "Expiration": {
                    "Days": ${expirationTime}
                }
              }
           ]
         }
      '';
    };

    ec2SecurityGroups.secGroup = {resources, ...}: {
      inherit region accessKeyId;
      rules = [
        { fromPort = 80;  toPort = 80;    sourceIp = "0.0.0.0/0"; }
        { fromPort = 22;  toPort = 22;    sourceIp = "0.0.0.0/0"; }
        { protocol = "-1"; fromPort = 0;   toPort = 65535; sourceIp = "172.31.0.0/16"; } ## all trusted inside the network
      ];
    };
  };

  app = { nodes, config, pkgs, resources, ... }:{

    deployment = {
      targetEnv = "ec2";
      ec2 = {
        inherit region accessKeyId;
        instanceType             = "t2.nano";
        associatePublicIpAddress = true;
        ebsInitialRootDiskSize   = 10;
        keyPair                  = resources.ec2KeyPairs.keyPair;
        securityGroups           = [resources.ec2SecurityGroups.secGroup];
      };
    };

    environment.systemPackages = [
      pgrst
      pgPkg
      pkgs.nixops
    ];

    time.timeZone = localTime;

    networking = {
      extraHosts =
        let pgHost = if primary
          then nodes.pg.config.networking.privateIPv4
          else nodes.pg-secondary.config.networking.privateIPv4;
        in ''
          ${pgHost} pg
        '';
      firewall.allowedTCPPorts = [ 80 ];
    };

    systemd.services.postgrest = {
      enable      = true;
      description = "postgrest daemon";
      wantedBy    = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart =
          let pgrstConf = pkgs.writeText "pgrst.conf" ''
            db-uri = "postgres://postgres@pg/postgres"
            db-schema = "public"
            db-anon-role = "postgres"

            server-port = 80
          '';
          in
          "${pgrst}/bin/postgrest ${pgrstConf}";
        Restart = "always";
      };
      restartTriggers = [ config.networking.extraHosts ];
    };
  };

  pg = { config, pkgs, resources, ... }:
  {
    time.timeZone = localTime;

    environment.systemPackages = [
      pkgs.awscli
      pkgs.gzip
    ];

    services.postgresql = {
      enable = primary;
      package = pgPkg;
      authentication = ''
        local replication postgres trust
        host  all all 172.31.0.0/16 trust
      '';
      settings = {
        log_statement = "all";
        wal_level = "archive";
        archive_mode = "on";
        archive_command = "${pkgs.gzip}/bin/gzip -c %p | ${pkgs.awscli}/bin/aws s3 cp - s3://${resources.s3Buckets.backups.name}/%f.gz 1>&2";
        archive_timeout = walTimeout;
        max_wal_senders = 10;
        timezone = localTime;
      } //
      pkgs.lib.optionalAttrs shouldRecover
      {
        restore_command = "cp ${config.services.postgresql.dataDir}/pg_wal/%f %p";
        recovery_target_time = recoveryTime;
      };
      enableTCPIP = true; ## Equivalent to listen_addresses = "*", for allowing TCP/IP connections
      initialScript = if (!shouldRecover) then ./sql/world.sql else null;
    };

  } // common {inherit region accessKeyId pkgs secrets shouldRecover pgPkg config resources; isActive = primary; };

  pg-secondary = { config, pkgs, resources, ... }:
  {

    environment.systemPackages = [
      pkgs.awscli
    ];

    time.timeZone = localTime;

    services.postgresql = {
      enable = secondary;
      package = pgPkg;
      authentication = ''
        local replication postgres trust
        host  all all 172.31.0.0/16 trust
      '';
      settings = {
        log_statement = "all";
        wal_level = "archive";
        archive_mode = "on";
        archive_command = "${pkgs.gzip}/bin/gzip -c %p | ${pkgs.awscli}/bin/aws s3 cp - s3://${resources.s3Buckets.backups.name}/%f.gz 1>&2";
        archive_timeout = walTimeout;
        restore_command = "cp ${config.services.postgresql.dataDir}/pg_wal/%f %p";
        max_wal_senders = 10;
        timezone = localTime;
        recovery_target_time = recoveryTime;
      };
      enableTCPIP = true; ## Equivalent to listen_addresses = "*", for allowing TCP/IP connections
    };

  } // common {inherit region accessKeyId pkgs secrets shouldRecover pgPkg config resources; isActive = secondary; };
}
