let
  nixpkgs = builtins.fetchTarball {
    name = "nixos-20.03";
    url = "https://github.com/nixos/nixpkgs/archive/7bc3a08d3a4c700b53a3b27f5acd149f24b931ec.tar.gz";
    sha256 = "1kiz37052zsgvw7a378zg08mpbi1wk8dkgm5j6dy0x4mxvcg8ws3";
  };
  pkgs = import nixpkgs {};
  deploy =
    pkgs.writeShellScriptBin "devops-dr-deploy"
      ''
        set -euo pipefail

        set +e && nixops info -d devops-dr > /dev/null 2> /dev/null
        info=$? && set -e

        if test $info -eq 1
        then
          echo "Creating deployment..."
          nixops create ./deploy.nix -d devops-dr
        fi

        nixops deploy -k -d devops-dr --confirm
      '';
  downloadNixopsState =
    pkgs.writeShellScriptBin "dl-nixops-state"
      ''
        set -euo pipefail

        aws s3 cp s3://$NIXOPS_STATE_BUCKET/deployment.nixops .
      '';
  uploadNixopsState =
    pkgs.writeShellScriptBin "up-nixops-state"
      ''
        set -euo pipefail

        aws s3 cp deployment.nixops s3://$NIXOPS_STATE_BUCKET
      '';
in
pkgs.mkShell {
  buildInputs = [
    pkgs.nixops
    downloadNixopsState
    deploy
    uploadNixopsState
  ];
  # Needed by nixops to use the pinned version and to store the state locally
  shellHook = ''
    export NIX_PATH="nixpkgs=${nixpkgs}:."
    export NIXOPS_STATE="deployment.nixops"
    source .env || true
  '';
}
